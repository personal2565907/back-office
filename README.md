# Back Office 
Employee management web app with angular.

## Try out the demo!
- You can demo the app here: [https://back-office-thariq.netlify.app/](https://back-office-thariq.netlify.app/).
- You can login with this credentials:
```
username: user1
password: pass1
```
- You can register on the web app demo to create new credentials.

## How to run the app on local
### Requirements 
- node version `^18.19.0`

### Steps
1. Clone the repo
`git clone  https://gitlab.com/personal2565907/back-office.git`
2. Go to back-office folder & Install dependencies
`cd back-office && npm i`
3. Run the app
`npm start`
4. The app will run on `http://localhost:4200/`
