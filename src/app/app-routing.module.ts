import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/auth/login/login.component';
import { EmployeeDetailComponent } from './pages/employee/employee-detail/employee-detail.component';
import { CreateEmployeeComponent } from './pages/employee/create-employee/create-employee.component';
import { EmployeeListComponent } from './pages/employee/employee-list/employee-list.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'employee-list', component: EmployeeListComponent, canActivate: [AuthGuard]},
  {path: 'create-employee', component: CreateEmployeeComponent, canActivate: [AuthGuard]},
  {path: 'employee-detail/:username', component: EmployeeDetailComponent, canActivate: [AuthGuard]},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
