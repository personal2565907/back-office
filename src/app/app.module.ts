import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { EmptyLayoutComponent } from './layout/empty-layout/empty-layout.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { CreateEmployeeComponent } from './pages/employee/create-employee/create-employee.component';
import { EmployeeDetailComponent } from './pages/employee/employee-detail/employee-detail.component';
import { EmployeeListComponent } from './pages/employee/employee-list/employee-list.component';

import { ToggleButtonModule } from 'primeng/togglebutton';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { ToastModule } from 'primeng/toast';
import { CardModule } from 'primeng/card';
import { CurrencyFormatPipe } from './pipes/currency-format.pipe';
import { FormsModule } from '@angular/forms';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { MessageService } from 'primeng/api';
import { ThemeService } from './services/theme/theme.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    CreateEmployeeComponent,
    EmployeeDetailComponent,
    EmployeeListComponent,
    AppLayoutComponent,
    EmptyLayoutComponent,
    CurrencyFormatPipe,
  ],
  imports: [
    ToggleButtonModule,
    CardModule,
    ToastModule,
    TagModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    ButtonModule,
    InputTextModule,
    PasswordModule,
    RouterModule,
    TableModule,
    FormsModule,
    DropdownModule,
    PanelModule,
    CalendarModule
  ],
  providers: [MessageService, ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
