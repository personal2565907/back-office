import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { Nullable } from 'primeng/ts-helpers';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  providers: [AuthService]
})
export class LoginComponent {

  constructor(
    private authService: AuthService,
    private messageService: MessageService,
    private router: Router
  ) { }

  login(username: string, password: Nullable<string>) {
    const isLoginSuccess = this.authService.login(username, password);
    if (isLoginSuccess) {
      this.router.navigate(['/employee-list']);
    } else {
      this.messageService.add({ severity: 'error', summary: 'Login Failed', detail: `Wrong username & password combination` });
    }
  }
}
