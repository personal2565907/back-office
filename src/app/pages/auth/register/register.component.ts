import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Nullable } from 'primeng/ts-helpers';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  constructor(
    private authService: AuthService,
    private messageService: MessageService,
    private router: Router
  ) { }

  register(username: string, password: Nullable<string>) {
    if (password) {
      this.authService.register(username, password);
      this.messageService.add({ severity: 'success', summary: 'Register Success' });
      this.router.navigate(['/login']);
    } else {
      this.messageService.add({ severity: 'error', summary: 'Password required!' });
    }
  }
}
