import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../models/employee/employee';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../../../services/employee/employee.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrl: './employee-detail.component.css',
  providers: [EmployeeService]
})
export class EmployeeDetailComponent implements OnInit {
  employee: Employee | undefined; // Define a variable to hold employee data

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,

  ) { }

  ngOnInit(): void {
    const username = this.route.snapshot.paramMap.get('username');
    this.employee = this.employeeService.getEmployees().find((emp) => emp.username === username)
  }
}
