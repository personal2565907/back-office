import { Component } from '@angular/core';
import { Employee } from '../../../models/employee/employee';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EmployeeService } from '../../../services/employee/employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrl: './create-employee.component.css',
  providers: [EmployeeService]
})
export class CreateEmployeeComponent {
  maxDate: Date;
  employee: Employee;
  statusOptions: string[];
  groupOptions: string[];

  constructor(
    private employeeService: EmployeeService,
    private messageService: MessageService,
    private router: Router
  ) {
    this.maxDate = new Date();
    this.employee = new Employee('', '', '', '', new Date(), 5000000, '');
    this.statusOptions = ['Active', 'Inactive', 'On Leave', 'Terminated', 'Retired'];
    this.groupOptions = ['IT', 'HR', 'Sales', 'Marketing', 'Finance', 'Operations', 'Administration', 'Engineering', 'Customer Service', 'Other'];
  }

  save(employee: Employee) {
    console.log({ employee })
    this.employeeService.addEmployee(employee)
    this.messageService.add({ severity: 'success', summary: 'Success Create', detail: `${employee.username} data has been saved` });
    this.router.navigate(['/employee-list']);
  }

  cancel() {
    this.router.navigate(['/employee-list']);
  }
}
