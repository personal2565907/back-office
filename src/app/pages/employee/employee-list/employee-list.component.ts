import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { EmployeeService } from '../../../services/employee/employee.service';
import { Employee } from '../../../models/employee/employee';
import { Table } from 'primeng/table';
import { MessageService } from 'primeng/api';
import { getSearchKey, setSearchKey } from '../../../constants';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.css',
  providers: [AuthService, EmployeeService]
})
export class EmployeeListComponent implements OnInit {
  username: string = '';
  employees: Employee[] = [];
  searchKey: string = '';

  @ViewChild('dt') table!: Table;

  constructor(
    private authService: AuthService,
    private employeeService: EmployeeService,
    private messageService: MessageService,
    private router: Router
  ) { }

  ngAfterViewInit(): void {
    const searchKey = getSearchKey()
    if (searchKey) {
      this.table.filterGlobal(searchKey, 'contains')
      this.setSearchKey(searchKey)
    }
  }
  ngOnInit(): void {
    const currentUser = this.authService.getCurrentUser();
    this.username = currentUser ? currentUser.username : '';
    this.employees = this.employeeService.getEmployees();
  }

  clear(table: Table) {
    table.clear();
  }

  filterGlobal(event: Event) {
    const searchKey = (event.target as HTMLInputElement).value
    this.table.filterGlobal(searchKey, 'contains')
    this.setSearchKey(searchKey)
  }

  setSearchKey(searchKey: string) {
    this.searchKey = searchKey
    setSearchKey(searchKey)
  }

  getStatus(status: string) {
    switch (status) {
      case 'Active':
        return 'success';
      case 'Inactive':
        return 'secondary';
      case 'On Leave':
        return 'help';
      case 'Terminated':
        return 'warning';
      case 'Retired':
        return 'danger';
      default:
        return 'success';
    }
  }

  createEmployee() {
    this.router.navigate(['/create-employee']);
  }

  employeDetail(username: string) {
    this.router.navigate([`/employee-detail`, username]);
  }

  editNotification(employee: Employee) {
    this.messageService.add({ icon: 'pi-check', severity: 'warn', summary: 'Success Edit', detail: `${employee.username} data has been changed` });
  }

  deleteNotification(employee: Employee) {
    this.messageService.add({ icon: 'pi-check', severity: 'error', summary: 'Success Delete', detail: `${employee.username} data has been deleted` });
  }
}
