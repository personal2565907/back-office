import { Employee } from "./models/employee/employee";
import { User } from "./models/user/user"
import { faker } from '@faker-js/faker';

export const USERS: User[] = [
  new User('user1', 'pass1'),
  new User('user2', 'pass2'),
  new User('user3', 'pass3'),
]

export const GROUPS = [
  'IT',
  'HR',
  'Sales',
  'Marketing',
  'Finance',
  'Operations',
  'Administration',
  'Engineering',
  'Customer Service',
  'Other'
]

export const STATUS = [
  'Active',
  'Inactive',
  'On Leave',
  'Terminated',
  'Retired'
]

let CURRENT_USER : User | undefined;

export const setCurrentUser = (user: User | undefined) => { CURRENT_USER = user }
export const getCurrentUser = () => CURRENT_USER

let EMPLOYEES: Employee[] = [];
export const getEmployees = () => EMPLOYEES
export const addEmployee = (employee: Employee) => EMPLOYEES.push(employee)
const generateEmployees = () => {
  for (let i = 0; i < 100; i++) {
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const username = `${firstName}${lastName[0]}${i + 1}`
    EMPLOYEES.push(new Employee(
      username,
      firstName,
      lastName,
      faker.internet.email({ firstName, lastName }),
      faker.date.birthdate(),
      Math.round((Math.random() * (10000000 - 5000000) + 5000000) * Math.pow(10, 2)) / Math.pow(10, 2),
      faker.commerce.productDescription()
    ))
  }
}

generateEmployees()

let SEARCH_KEY: string = ''
export const setSearchKey = (key: string) => { SEARCH_KEY = key }
export const getSearchKey = () => SEARCH_KEY


let THEME: string = 'light'
export const setTheme = (theme: string) => { THEME = theme }
export const getTheme = () => THEME
