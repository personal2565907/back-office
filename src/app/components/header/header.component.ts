import { Component } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { ThemeService } from '../../services/theme/theme.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
  providers: [AuthService, ThemeService]
})
export class HeaderComponent {
  username: string = '';

  constructor(
    private authService: AuthService,
    private themeService: ThemeService,
    private router: Router
  ) {
    const user = this.authService.getCurrentUser()
    this.username = user ? user.username : ''
  }

  logout() {
    this.authService.logout()
    this.router.navigate(['/login']);
  }

  changeTheme() {
    this.themeService.switchTheme()
  }

}
