import { GROUPS, STATUS } from "../../constants";

type double = number

export class Employee {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: double;
  status: string;
  group: string;
  description: string;

  constructor(
    username: string,
    firstName: string,
    lastName: string,
    email: string,
    birthDate: Date,
    basicSalary: double,
    description: string
  ) {
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.birthDate = birthDate;
    this.basicSalary = basicSalary;
    this.status = STATUS[Math.floor(Math.random() * 5)];
    this.group =  GROUPS[Math.floor(Math.random() * 10)];
    this.description = description;
  }

}
