import { Injectable } from '@angular/core';
import { addEmployee, getEmployees } from '../../constants';
import { Employee } from '../../models/employee/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() {}

  getEmployees() {
    return getEmployees()
  }

  addEmployee(employee: Employee) {
    addEmployee(employee)
  }

}
