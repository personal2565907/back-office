import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { getTheme, setTheme } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(@Inject(DOCUMENT) private document: Document
  ) { }

  switchTheme() {
    const themeElem = this.document.getElementById('app-theme') as HTMLLinkElement
    if (themeElem) {
      const currentTheme = getTheme()
      if (currentTheme.includes('light')) {
        setTheme('dark')
        themeElem.href = `lara-dark-indigo.css`
      } else {
        setTheme('light')
        themeElem.href = `lara-light-indigo.css`
      }
    }
  }

}
