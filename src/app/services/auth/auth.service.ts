import { Injectable } from '@angular/core';
import { User } from '../../models/user/user';
import { Nullable } from 'primeng/ts-helpers';
import { USERS, setCurrentUser, getCurrentUser } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuthenticated(): boolean {
    return getCurrentUser() !== undefined
  }

  getCurrentUser () {
    return getCurrentUser()
  }

  login(username: string, password: Nullable<string>): boolean {
    setCurrentUser(USERS.find(user => user.username === username && user.password === password))
    return getCurrentUser() !== undefined
  }

  logout() {
    setCurrentUser(undefined)
  }

  register(username: string, password: string) {
    USERS.push(new User(username, password))
  }
}
